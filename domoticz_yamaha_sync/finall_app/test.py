import requests
import time
from requests.packages.urllib3.exceptions import InsecureRequestWarning

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class Yamaha(object):
    yamaha_ip = '192.168.90.203'
    domoticz_ip = '192.168.90.205:9443'
    domoticz_auth = ('smietek92', 'Wakacje!12')
    yamaha_url = None

    idx = {
        'power': 3,
        'mute': 6,
        'input': 2
    }

    input_levels = {
        'hdmi1': 20,
        'hdmi2': 10,
        'spotify': 30,
        'net_radio': 40,
        'hdmi3': 50,
        'bluetooth': 60
    }
    power_levels = {
        'on': 'On',
        'standby': 'Off'
    }
    domoticz_power_bool = {
        'On': True,
        'Off': False
    }
    yamaha_power_bool = {
        'on': True,
        'standby': False
    }

    def __init__(self, *argv, **kwargs):
        super(Yamaha, self).__init__(*argv, **kwargs)

        self.yamaha_url = 'http://{0}/YamahaExtendedControl/v1/main/' \
            .format(self.yamaha_ip)

    def get_yamaha_status(self):
        try:
            r = requests.get('{0}getStatus'.format(self.yamaha_url))
            return r.json()
        except requests.exceptions.ConnectionError:
            return None

    def get_domoticz_status(self, rid=0):
        r = self.domoticz_get(
            url=self.generate_domoticz_url(
                type='devices', rid=rid))
        return r.json()

    def get_domoticz_power(self):
        try:
            return self.domoticz_power_bool[self.get_domoticz_status(
                rid=self.idx['power'])['result'][0]['Data']]
        except KeyError:
            return False

    def get_yamaha_power(self):
        try:
            power = self.get_yamaha_status()['power']
            if power:
                return self.yamaha_power_bool[power]
            else:
                return False
        except KeyError:
            return False

    def get_yamaha_mute(self):
        try:
            return self.get_yamaha_status()['mute']
        except KeyError:
            return False

    def get_domoticz_mute(self):
        try:
            return self.domoticz_power_bool[self.get_domoticz_status(
                rid=self.idx['mute'])['result'][0]['Data']]
        except KeyError:
            return False

    def get_yamaha_input(self):
        try:
            return self.input_levels[self.get_yamaha_status()['input']]
        except KeyError:
            return False

    def get_domoticz_input(self):
        try:
            return self.domoticz_power_bool[self.get_domoticz_status(
                rid=self.idx['input'])['result'][0]['Level']]
        except KeyError:
            return False

    def set_domoticz_switch(self, value=False, idx=0, level=False):
        if level:
            self.domoticz_get(url=self.generate_domoticz_url(
                type='command',
                param='switchlight',
                idx=idx,
                switchcmd='Set%20Level',
                level=level))
        else:
            self.domoticz_get(url=self.generate_domoticz_url(
                type='command',
                param='switchlight',
                idx=idx,
                switchcmd=('On' if value else 'Off')))

    def update_domoticz(self):
        # update power
        if self.get_yamaha_power() != self.get_domoticz_power():
            self.set_domoticz_switch(
                value=self.get_yamaha_power(), idx=self.idx['power'])

        # update mute
        if self.get_yamaha_mute() != self.get_domoticz_mute():
            self.set_domoticz_switch(
                value=self.get_yamaha_mute(), idx=self.idx['mute'])

        # update input
        if self.get_yamaha_input() != self.get_domoticz_input():
            self.set_domoticz_switch(
                level=self.get_yamaha_input(), idx=self.idx['input'])

    def domoticz_get(self, url):
        try:
            return requests.get(url, auth=self.domoticz_auth, verify=False)
        except requests.exceptions.ConnectionError:
            return None

    def generate_domoticz_url(self, **kwargs):
        url = 'https://{0}/json.htm?'.format(self.domoticz_ip)

        for key in kwargs:
            url = '{0}{1}={2}&'.format(url, key, kwargs[key])

        url = url[:-1]
        return url


if __name__ == '__main__':
    yamaha = Yamaha()
    # yamaha.update_domoticz()
    while True:
        yamaha.update_domoticz()
        time.sleep(600)
