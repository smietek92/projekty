Repozytorium zawiera dwie aplikacje, które są częścią projektu inteligentnego domu.

---

## domoticz_gui (rozszerzenie funkcjonalności systemu Inteligentnego Domu - domoticz + MySensors)

Jest to aplikacja desktopowa, za pomocą której mamy dostęp naszego panelu domoticz. Układ paneli oraz rozmieszczenie elementów definiujemy w pliku konfiguracyjnym **domoticz_config.json**

Pomijacąc GUI dostarczane przez domoticz, które jest dostępne w wersji przeglądarkowej, chciałem utworzyć aplikację dzięki której będę miał dostęp do zarządzania bez potrzeby wyszukiwania odpowiedniej zakładki w przeglądarce. 

**Nie jest to produkt końcowy!!!**

---

## domoticz_yamaha_sync

Aplikacja ta odpowiada za synchronizację amplitunera oraz danych systemu domoticz w sytuacji, kiedy użytkownik zdecydował się na obsługę pilotem zamiast aplikacją.