from requests.packages.urllib3.exceptions import InsecureRequestWarning
import requests

requests.packages.urllib3.disable_warnings(InsecureRequestWarning)


class Domoticz(object):
    ip = '192.168.90.205'
    port = 9443
    auth = ('smietek92', 'Wakacje!12')
    idx = {
        'power': 3,
        'mute': 6,
        'input': 2
    }
    power_bool = {
        'On': True,
        'Off': False
    }

    def __init__(self, *argv, **kwargs):
        self.ip = kwargs.pop('ip', '0.0.0.0')
        self.port = kwargs.pop('port', '443')
        username = kwargs.pop('username', '')
        password = kwargs.pop('password', '')
        self.auth = (username, password)
        super(Domoticz, self).__init__(*argv, **kwargs)

    def set_dataa(self, *argv, **kwargs):
        self.ip = kwargs.pop('ip', '0.0.0.0')
        self.port = kwargs.pop('port', '443')
        username = kwargs.pop('username', '')
        password = kwargs.pop('password', '')
        self.auth = (username, password)

    def get_input_levels(self):
        return self.input_levels

    def set_input_levels(self, input_levels={}):
        self.input_levels = input_levels

    def get_status(self, rid=0):
        r = self.get(
            url=self.generate_url(
                type='devices', rid=rid))
        return r.json()

    def get_temperature(self, rid=0):
        return self.get_status(rid=rid)['result'][0]['Temp']

    def get_power(self):
        try:
            return self.power_bool[self.get_status(
                rid=self.idx['power'])['result'][0]['Data']]
        except KeyError:
            return False

    def get_mute(self):
        try:
            return self.power_bool[self.get_status(
                rid=self.idx['mute'])['result'][0]['Data']]
        except KeyError:
            return False

    def is_awake(self):
        return True if self.get(url=self.generate_url(
            type='command', param='getversion')) else False

    def get_input(self):
        try:
            return self.get_status(
                rid=self.idx['input'])['result'][0]['Level']
        except KeyError:
            return False

    def set_switch(self, value=False, idx=0, level=False, toggle=False):
        if level:
            self.get(url=self.generate_url(
                type='command',
                param='switchlight',
                idx=idx,
                switchcmd='Set%20Level',
                level=level))
        elif toggle:
            self.get(url=self.generate_url(
                type='command',
                param='switchlight',
                idx=idx,
                switchcmd='Toggle'))
        else:
            self.get(url=self.generate_url(
                type='command',
                param='switchlight',
                idx=idx,
                switchcmd=('On' if value else 'Off')))

    def set_scene(self, idx=0):
        self.get(url=self.generate_url(
            type='command',
            param='switchscene',
            idx=idx,
            switchcmd='On'))

    def get_scene_children(self, idx=0):
        r = self.get(url=self.generate_url(
            type='command',
            param='getscenedevices',
            idx=idx,
            isscene='true'))

        return [int(data['DevID']) for data in r.json()['result']]

    def get(self, url):
        try:
            return requests.get(url, auth=self.auth, verify=False)
        except requests.exceptions.ConnectionError:
            return None

    def generate_url(self, **kwargs):
        url = 'https://{0}:{1}/json.htm?'.format(self.ip, self.port)

        for key in kwargs:
            url = '{0}{1}={2}&'.format(url, key, kwargs[key])

        return url[:-1]


if __name__ == '__main__':
    domoticz = Domoticz()
    print(domoticz.get_status(rid=13))
