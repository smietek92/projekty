import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QWidget, QFrame
from domoticz import Domoticz
import json
import logging
import os

BASE_DIR = os.path.dirname(__file__)

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s',
                    level=logging.DEBUG, datefmt='%d-%m-%Y %H:%M:%S')

WINDOW_WIDTH = 300
WINDOW_HEIGHT = 400
SMALL_WINDOW_WIDTH = 100
SMALL_WINDOW_HEIGHT = 100

SCREEN_WIDTH = 0
SCREEN_HEIGHT = 0

INPUT_VALUES = {
    "hdmi2": 10,
    "hdmi1": 20,
    "spotify": 30,
    "radio": 40,
    "ps4": 50,
    "bluetooth": 60,
    "5ch": 10
}

domoticz = Domoticz()


class LocationTree():
    current_location = None
    history = []

    def next(self, frame=None):
        self.current_location.hide()
        self.history.append(self.current_location)
        self.current_location = frame
        self.current_location.show()

    def previous(self):
        if len(self.history) == 0:
            logging.info('Exiting')
            sys.exit(0)

        self.current_location.hide()
        self.current_location = self.history.pop()
        self.current_location.show()

    def set_current_location(self, current_location):
        self.current_location = current_location

    def set_history(self, history):
        self.history = history


location_tree = LocationTree()


class ButtonBasicLayout():
    button_height = 40

    def __init__(self, *argv, **kwargs):
        self.setMinimumHeight(self.button_height)
        super(ButtonBasicLayout, self).__init__(*argv, **kwargs)


class ImageLayout(QLabel):
    def __init__(self, path=None, *argv, **kwargs):
        self.kwargs = kwargs

        if path is None:
            logging.error("'image' type need 'path'")
            logging.error('Exiting')
            sys.exit(0)

        super(ImageLayout, self).__init__()

        self.setStyleSheet(
            "background-image: url('{0}'); background-attachment: fixed"
            .format(os.path.join(BASE_DIR, path).replace('\\', '/')))


class CounterLayout(QLabel):
    def __init__(self, *argv, **kwargs):
        self.kwargs = kwargs

        super(CounterLayout, self).__init__('counter')


class ExitLayout(QPushButton, ButtonBasicLayout):
    def __init__(self, label='Exit', *argv, **kwargs):
        self.kwargs = kwargs

        super(ExitLayout, self).__init__(label)

        self.clicked.connect(self.on_cancel)

    def on_cancel(self):
        logging.info("Exiting.")
        sys.exit(0)


class NavigateLayout(QPushButton, ButtonBasicLayout):
    def __init__(self, label='Navigate', target=None, *argv, **kwargs):
        self.kwargs = kwargs
        self.target = target

        super(NavigateLayout, self).__init__(label)

        self.clicked.connect(self.switch_frame)

    def switch_frame(self):
        location_tree.next(frame=self.parent().parent().frames[self.target])


class LabelLayout(QLabel):
    def __init__(self, label='LabelLayout', *argv, **kwargs):
        self.kwargs = kwargs

        super(LabelLayout, self).__init__(label)


class TempLayout(QLabel):
    def __init__(self, label='LabelLayout', idx=None, *argv, **kwargs):
        self.kwargs = kwargs
        self.idx = idx

        if idx is None:
            logging.error("'temp' type need 'idx'")
            logging.error('Exiting')
            sys.exit(0)

        super(TempLayout, self).__init__(label)

        myFont = QFont()
        myFont.setBold(True)
        self.setFont(myFont)

    def init_data(self):
        temp = "{0} °C".format(domoticz.get_temperature(rid=self.idx))
        self.setText(temp)


class BackLayout(QPushButton, ButtonBasicLayout):
    def __init__(self, label='BackLayout', *argv, **kwargs):
        self.kwargs = kwargs

        super(BackLayout, self).__init__(label)

        self.clicked.connect(self.back_func)

    def back_func(self):
        location_tree.previous()


class ToggleLayout(QPushButton, ButtonBasicLayout):
    def __init__(self, label='ToggleLayout', idx=None, *argv, **kwargs):
        self.kwargs = kwargs
        self.idx = idx

        if idx is None:
            logging.error("'toggle' type need 'idx'")
            logging.error('Exiting')
            sys.exit(0)

        super(ToggleLayout, self).__init__(label)

        self.setCheckable(True)
        self.clicked.connect(self.click_fun)

    def click_fun(self, e):
        if self.isChecked():
            domoticz.set_switch(value=True, idx=self.idx)
        else:
            domoticz.set_switch(value=False, idx=self.idx)

    def init_data(self):
        status = domoticz.get_status(rid=self.idx)['result'][0]['Data']
        status = True if status == 'On' else True if status \
            .startswith('Set Level') else False

        if status != self.isChecked():
            self.toggle()


class ClickOnLayout(QPushButton, ButtonBasicLayout):
    def __init__(self, label='ClickOnLayout', idx=None, *argv, **kwargs):
        self.kwargs = kwargs
        self.idx = idx

        if idx is None:
            logging.error("'click_on' type need 'idx'")
            logging.error('Exiting')
            sys.exit(0)

        super(ClickOnLayout, self).__init__(label)

        self.clicked.connect(self.click_fun)

    def click_fun(self, e):
        domoticz.set_switch(idx=self.idx, value=True)


class ClickOffLayout(QPushButton, ButtonBasicLayout):
    def __init__(self, label='ClickOffLayout', idx=None, *argv, **kwargs):
        self.kwargs = kwargs

        if idx is None:
            logging.error("'click_off' type need 'idx'")
            logging.error('Exiting')
            sys.exit(0)

        self.clicked.connect(
            lambda: domoticz.get(url=domoticz.generate_url(
                type='command',
                param='switchlight',
                idx=idx,
                switchcmd='Off')))


class SceneLayout(QPushButton, ButtonBasicLayout):
    def __init__(self, label='SceneLayout', idx=None, *argv, **kwargs):
        self.kwargs = kwargs
        self.idx = idx

        if idx is None:
            logging.error("'scene' type need 'idx'")
            logging.error('Exiting')
            sys.exit(0)

        super(SceneLayout, self).__init__(label)

        self.clicked.connect(self.click_fun)

    def click_fun(self, e):
        domoticz.set_scene(idx=self.idx)
        ids = domoticz.get_scene_children(idx=self.idx)

        for row in self.parent().layout_set:
            for col in row:
                try:
                    if col.object_output.idx in ids:
                        col.object_output.init_data()
                except AttributeError:
                    pass


class DimmerLayout(QLabel):
    def __init__(self, *argv, **kwargs):

        super(DimmerLayout, self).__init__(*argv, **kwargs)


class SelectLayout(QPushButton, ButtonBasicLayout):
    def __init__(self, value='', label='SelectLayout', previous=True, idx=None,
                 *argv, **kwargs):
        self.previous = previous
        self.kwargs = kwargs
        self.value = value
        self.idx = idx

        if idx is None:
            logging.error("'select' type need 'idx'")
            logging.error('Exiting')
            sys.exit(0)

        super(SelectLayout, self).__init__(label)

        self.clicked.connect(self.click_fun)

    def click_fun(self, e):
        domoticz.set_switch(level=INPUT_VALUES[self.value],
                            idx=self.idx)

        if self.previous:
            location_tree.previous()


LAYOUT_TYPE = {
    'toggle': ToggleLayout,
    'temp': TempLayout,
    'dimmer': DimmerLayout,
    'click_on': ClickOnLayout,
    'click_off': ClickOffLayout,
    'label': LabelLayout,
    'exit': ExitLayout,
    'image': ImageLayout,
    'navigate': NavigateLayout,
    'back': BackLayout,
    'scene': SceneLayout,
    'select': SelectLayout,
    'counter': CounterLayout
}


class CustomFrame(QFrame):
    small = False
    layout_set = None

    def __init__(self, *argv, **kwargs):
        self.layout_set = kwargs.pop('layout_set', [])
        self.margin = kwargs.pop('margin', None)
        small = kwargs.pop('small', self.small)
        x = kwargs.pop('x', 0)
        y = kwargs.pop('y', 0)
        hide = kwargs.pop('hide', True)

        super(CustomFrame, self).__init__(*argv, **kwargs)

        self.set_geometry(small=small, x=x, y=y)
        self.setFrameStyle(QFrame.NoFrame | QFrame.Panel)
        if hide:
            self.hide()

        self.init_UI()

    def init_UI(self):
        self.conver_to_LayoutSet()

        grid = QGridLayout()
        if self.margin is not None:
            grid.setContentsMargins(self.margin, self.margin,
                                    self.margin, self.margin)
        self.setLayout(grid)

        for row in self.layout_set:
            for col in row:
                grid.addWidget(
                    col.object_output,
                    col.r,
                    col.c,
                    col.rowspan,
                    col.colspan)

    def conver_to_LayoutSet(self):
        tmp_layout_set = []
        r_index = 0
        for row in self.layout_set:
            row_layout = []
            c_index = 0
            for col in row:
                row_layout.append(LayoutSet(r=r_index, c=c_index,
                                            parent=self,
                                            **col))
                c_index += col.get('colspan', 1)
            tmp_layout_set.append(row_layout)
            r_index += 1

        self.layout_set = tmp_layout_set

    def set_geometry(self, small=False, x=None, y=None):
        if small:
            x = x if x is not None else (SCREEN_WIDTH - SMALL_WINDOW_WIDTH)
            y = y if y is not None else SCREEN_HEIGHT - SMALL_WINDOW_HEIGHT \
                - 50
            height = SMALL_WINDOW_HEIGHT
            width = SMALL_WINDOW_WIDTH
        else:
            x = x if x is not None else SCREEN_WIDTH - WINDOW_WIDTH
            y = y if y is not None else SCREEN_HEIGHT - WINDOW_HEIGHT - 50
            height = WINDOW_HEIGHT
            width = WINDOW_WIDTH

        self.setGeometry(x, y, width, height)

    def update_data(self):
        if self.layout_set:
            for row in self.layout_set:
                for col in row:
                    try:
                        col.object_output.init_data()
                    except AttributeError:
                        pass

    def show(self, *argv, **kwargs):
        self.update_data()
        super(CustomFrame, self).show(*argv, **kwargs)


class LayoutSet(object):
    button_height = 40

    def __init__(self, *argv, **kwargs):
        ignore_in_error = ('parent')

        layout_type = kwargs.get('type', None)
        self.colspan = kwargs.pop('colspan', 1)
        self.rowspan = kwargs.pop('rowspan', 1)
        self.r = kwargs.pop('r', 0)
        self.c = kwargs.pop('c', 0)

        if layout_type is None:
            for data in list(kwargs):
                if data in ignore_in_error:
                    kwargs.pop(data)
            logging.error(
                "Must define 'type' of LayoutSet. Parsed arguments: {0}"
                .format(kwargs))
            logging.error("Exiting.")
            sys.exit(0)

        kwargs = self.init_layout(**kwargs)

        for data in list(kwargs):
            if data in ignore_in_error:
                kwargs.pop(data)
        if len(kwargs) > 0:
            logging.error("Have problem with: {0}.".format(kwargs))
            logging.error("Exiting.")
            sys.exit(0)

        super(LayoutSet, self).__init__()

    def init_layout(self, *argv, **kwargs):
        layout_type = kwargs.pop('type')

        self.object_output = LAYOUT_TYPE[layout_type](**kwargs)

        return self.object_output.kwargs


class Application(QWidget):
    opacity = 0.15
    timeout = 300
    json_layout = None
    connected = False
    frames = {}
    expanded = False

    def __init__(self, *argv, **kwargs):
        self.init_from_json()

        super(Application, self).__init__(*argv, **kwargs)

        self.init_UI()
        self.show()
        self.init_data()

    def init_from_json(self):
        global WINDOW_WIDTH
        global WINDOW_HEIGHT
        global SMALL_WINDOW_WIDTH
        global SMALL_WINDOW_HEIGHT

        config_file = 'domoticz_config.json'
        try:
            logging.info("Loading config file: {0}.".format(config_file))
            f = open(os.path.join(BASE_DIR, config_file), encoding='UTF-8')
            f.readline()
            self.json_layout = json.load(f)

            if 'w8_conn_frame' not in self.json_layout['frames']:
                logging.error("Define 'w8_conn_frame' in config file: {0}"
                              .format(config_file))
                logging.error('Exiting.')
                sys.exit(0)

            if 'dashboard_frame' not in self.json_layout['frames']:
                logging.error("Define 'dashboard_frame' in config file: {0}"
                              .format(config_file))
                logging.error('Exiting.')
                sys.exit(0)

            if 'domoticz_icon' not in self.json_layout['frames']:
                logging.error("Define 'domoticz_icon' in config file: {0}"
                              .format(config_file))
                logging.error('Exiting.')
                sys.exit(0)

            WINDOW_WIDTH = self.json_layout['window_width']
            WINDOW_HEIGHT = self.json_layout['widnow_height']
            SMALL_WINDOW_WIDTH = self.json_layout['small_window_width']
            SMALL_WINDOW_HEIGHT = self.json_layout['small_widnow_height']

            domoticz.set_dataa(
                ip=self.json_layout['domoticz_ip'],
                port=self.json_layout['domoticz_port'],
                username=self.json_layout['domoticz_username'],
                password=self.json_layout['domoticz_password'])

            logging.info("Config file loaded successfully!")
        except FileNotFoundError:
            logging.error("Can't find configuration file: {0}."
                          .format(config_file))
            logging.error("Exiting.")
            sys.exit(0)

    def init_UI(self):
        self.setWindowOpacity(self.opacity)
        self.setAutoFillBackground(False)
        self.set_geometry(small=True)
        self.setWindowFlags(Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint |
                            Qt.WA_TranslucentBackground)

        self.frames['domoticz_icon'] = CustomFrame(
            self, hide=False, margin=0,
            layout_set=self.json_layout['frames']['domoticz_icon'])
        self.frames['w8_conn_frame'] = CustomFrame(
            self, layout_set=self.json_layout['frames']['w8_conn_frame'])
        self.frames['dashboard_frame'] = CustomFrame(
            self, layout_set=self.json_layout['frames']['dashboard_frame'])

        for frame in self.json_layout['frames']:
            if frame not in ('domoticz_icon',
                             'w8_conn_frame',
                             'dashboard_frame'):
                self.frames[frame] = CustomFrame(
                    self, layout_set=self.json_layout['frames'][frame])

    def sleep(self, seconds=1):
        loop = QEventLoop()
        QTimer.singleShot(seconds * 1000, loop.quit)
        loop.exec_()

    def w8_for_conn(self, info_label=False):
        counter = 0
        while not domoticz.is_awake():
            if counter == self.timeout:
                return False
            if info_label:
                info_label.setText(
                    'Nie udało się połączyć z serwerem. Koniec podejmowania prób' +
                    ' za {0} {1}.'.format(
                        self.timeout - counter,
                        ('sekundę' if (self.timeout - counter) == 1 else 'sekund')))
            counter += 1
            self.sleep(1)
        return True

    def init_data(self):
        self.frames['domoticz_icon'].show()
        location_tree.set_current_location(self.frames['w8_conn_frame'])
        location_tree.set_history(history=[])
        location_tree.current_location.show()

        if self.w8_for_conn():
            self.connected = True
            location_tree.current_location.hide()
            location_tree.set_current_location(self.frames['dashboard_frame'])
            location_tree.set_history(
                history=[])
        else:
            logging.error('No connection to domoticz server!')
            logging.error('Exiting.')
            sys.exit(0)

    def min_window(self):
        location_tree.current_location.hide()

        if self.connected:
            location_tree.set_current_location(self.frames['dashboard_frame'])
            location_tree.set_history(history=[])

        self.frames['domoticz_icon'].show()

        self.setWindowOpacity(self.opacity)

        x = SCREEN_WIDTH - SMALL_WINDOW_WIDTH
        y = SCREEN_HEIGHT - SMALL_WINDOW_HEIGHT - 50
        height = SMALL_WINDOW_HEIGHT
        width = SMALL_WINDOW_WIDTH

        self.anim = QPropertyAnimation(self, b"geometry")
        self.anim.setDuration(150)
        self.anim.setStartValue(self.geometry())
        self.anim.setEndValue(QRect(x, y, width, height))
        self.anim.start()

        self.expanded = False

    def max_window(self):
        location_tree.current_location.show()
        self.frames['domoticz_icon'].hide()

        self.setWindowOpacity(1)

        x = SCREEN_WIDTH - WINDOW_WIDTH
        y = SCREEN_HEIGHT - WINDOW_HEIGHT - 50
        height = WINDOW_HEIGHT
        width = WINDOW_WIDTH

        self.anim = QPropertyAnimation(self, b"geometry")
        self.anim.setDuration(150)
        self.anim.setStartValue(self.geometry())
        self.anim.setEndValue(QRect(x, y, width, height))
        self.anim.start()

        self.expanded = True

    def enterEvent(self, QEvent):
        try:
            self.timer.stop()
        except AttributeError:
            pass
        if not self.expanded:
            self.max_window()

    def leaveEvent(self, QEvent):
        self.timer = QTimer()
        self.timer.timeout.connect(self.min_window)
        self.timer.setSingleShot(True)
        self.timer.start(1000)

    def set_geometry(self, small=False):
        if small:
            self.setGeometry(SCREEN_WIDTH - SMALL_WINDOW_WIDTH,
                             SCREEN_HEIGHT - SMALL_WINDOW_HEIGHT - 50,
                             SMALL_WINDOW_WIDTH,
                             SMALL_WINDOW_HEIGHT)
        else:
            self.setGeometry(SCREEN_WIDTH - WINDOW_WIDTH,
                             SCREEN_HEIGHT - WINDOW_HEIGHT - 50,
                             WINDOW_WIDTH,
                             WINDOW_HEIGHT)


if __name__ == '__main__':
    logging.info('Starting app.')
    app = QApplication(sys.argv)
    screen_resolution = app.desktop().screenGeometry()
    SCREEN_WIDTH, SCREEN_HEIGHT = screen_resolution.width(), \
        screen_resolution.height()
    application = Application()
    sys.exit(app.exec_())
